const mongoose = require("mongoose");

const stockSchema = mongoose.Schema({
  Name: {
    type: String,
    required: true,
  },
  CurrentMarketPrice: {
    type: Number,
  },
  MarketCap: {
    type: Number,
  },
  StockPE: {
    type: Number,
  },
  DividendYield: {
    type: Number,
  },
  "ROCE%": {
    type: Number,
  },
  ROE_PreviousAnnum: {
    type: Number,
  },
  DebtToEquity: {
    type: Number,
  },
  EPS: {
    type: Number,
  },
  Reserves: {
    type: Number,
  },
  Debt: {
    type: Number,
  },

  CreatedOn: { type: Date },
  UpdatedOn: { type: Date },
});

const stock = mongoose.model("Stock", stockSchema);
module.exports = stock;
