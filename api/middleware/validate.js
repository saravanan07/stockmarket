const { body, param, check } = require("express-validator");
const messageConfig = require("../config/message.json");

module.exports = (method) => {
  switch (method) {
    case "signupUser":
      return [
        body("UserName", messageConfig.UserNameRequired.Message)
          .trim()
          .notEmpty(),
        body("Password", messageConfig.PasswordRequired.Message)
          .trim()
          .notEmpty()
          .matches("^[a-zA-Z0-9!@#$%^&*]{8,}$")
          .withMessage(messageConfig.PasswordFormatError.Message),
      ];
    case "loginUser":
      return [
        body("UserName", messageConfig.UserNameRequired.Message)
          .trim()
          .notEmpty(),

        body("Password", messageConfig.PasswordRequired.Message)
          .trim()
          .notEmpty(),
      ];
    case "fetchStockDetails":
      return [
        param("StockName", messageConfig.StockNameRequired.Message)
          .trim()
          .notEmpty(),
      ];

    default:
      break;
  }
};
