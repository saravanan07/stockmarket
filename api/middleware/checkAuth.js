const jwt = require("jsonwebtoken");
const config = require("../config/config");
const messageConfig = require("../config/message.json");
exports.checkAuth = (req, res, next) => {
  try {
    if (req.headers) {
      //   console.log(req.headers);
      const accessToken = req.headers.token;
      //   console.log(accessToken);
      // ******* validate access token  *****
      jwt.verify(accessToken, config.JwtKey, (err, decoded) => {
        if (!err) {
          req.userData = decoded;
          next();
        } else {
          console.log(err);
          res.status(401).json({
            Message: messageConfig.AuthenticationFailed.Message,
          });
        }
      });
    } else {
      res.status(401).json({
        Message: messageConfig.AuthenticationFailed.Message,
      });
    }
  } catch (error) {
    next(error);
    res.status(500).json({
      Message: messageConfig.ServerError.Message,
    });
  }
};
