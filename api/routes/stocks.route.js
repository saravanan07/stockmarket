const express = require("express");
const router = express.Router();
const validate = require("../middleware/validate");
const checkAuth = require("../middleware/checkAuth").checkAuth;

const StocksCOntroller = require("../controller/stock.controller");

router.get(
  "/fetch-stock/:StockName",
  checkAuth,
  validate("fetchStockDetails"),
  StocksCOntroller.fetchSingleStock
);

router.post("/get-stocks-name", checkAuth, StocksCOntroller.fetchStocksName);

module.exports = router;
