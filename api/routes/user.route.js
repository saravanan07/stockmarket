const express = require("express");
const router = express.Router();
const validate = require("../middleware/validate");

const UserController = require("../controller/user.controller");

router.post("/signup", validate("signupUser"), UserController.signupUser);
router.post("/login", validate("loginUser"), UserController.loginUser);

module.exports = router;
