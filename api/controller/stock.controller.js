const Stocks = require("../model/stocks");
const { validationResult, query } = require("express-validator");
const messageConfig = require("../config/message.json");

/**
 * fetch a stock's details
 *
 * @param {String} StockName Name of a stock
 * @return {Object} - Stock.
 */
exports.fetchSingleStock = (req, res, next) => {
  const errors = validationResult(req); // Finds the validation errors in this request and wraps them in an object with handy functions
  if (!errors.isEmpty()) {
    return res.status(400).json({
      Message: errors
        .array({ onlyFirstError: true })
        .map((x) => x.msg)
        .toString(),
    });
  }
  const stockName = req.params.StockName;
  if (stockName == undefined || stockName == "") {
    return res.status(400).json({ Message: messageConfig.StockNameRequired });
  }
  Stocks.findOne({ Name: stockName })
    .exec()
    .then((stock) => {
      if (!stock) {
        return res
          .status(404)
          .json({ Message: messageConfig.StockNotFound.Message });
      }
      res.status(201).json({
        Stock: stock,
      });
    })
    .catch((err) => {
      next(err);
      res.status(500).json({ Message: messageConfig.ServerError.Message });
    });
};

/**
 * get stock's name while searching
 *
 * @body {String} StockName Name of a stock
 * @return {Array} - Array of string Names.
 */
exports.fetchStocksName = (req, res, next) => {
  let keyword = "";
  if (req.body.Keyword) {
    keyword = new RegExp(RegExp.escape(req.body.Keyword), "i");
  }

  Stocks.find({ Name: keyword })
    .select("Name")
    .exec()
    .then((stocks) => {
      if (!stocks) {
        return res
          .status(404)
          .json({ Message: messageConfig.StockNotFound.Message });
      }
      res.status(201).json(stocks);
    })
    .catch((err) => {
      next(err);
      res.status(500).json({ Message: messageConfig.ServerError.Message });
    });
};
